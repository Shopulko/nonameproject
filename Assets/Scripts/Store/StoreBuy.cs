﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StoreBuy : MonoBehaviour {

    public TextMeshProUGUI btnText;
    public Image icon;
    public Item item;

	void Start () {
        btnText.text += " " + item.price;
        icon.sprite = item.icon;
	}
	
	
	public void OnBtnBuy()
    {
        if(GameManagers.instance.money >= item.price)
        {
            Inventory.instance.AddItem(item);
            GameManagers.instance.money -= item.price;
        }
        else
        {
            //Not enough money
            Debug.Log("Not enough money");
        }
    }

}
