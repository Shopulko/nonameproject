﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsScript : MonoBehaviour {

    public GameObject[] btnsControl;
    public GameObject pausePanel;

    GameManagers gameManagers;

    public GameObject InventoryPanel;
    public GameObject infoInventoryPanel;
    public GameObject storePanel;
    public GameObject lobbyPanel;
    public GameObject statsPanel;

    void Start()
    {
        gameManagers = GameManagers.instance;
        InventoryPanel.SetActive(false);
        infoInventoryPanel.SetActive(false);
        storePanel.SetActive(false);
        lobbyPanel.SetActive(false);
        statsPanel.SetActive(false);
    }


    public void OnPauseBtn()
    {
        for (int i = 0; i < btnsControl.Length; i++)
        {
            btnsControl[i].SetActive(false);
        }
        pausePanel.SetActive(true);
        PauseInGame();
    }

    public void OnResumeEnter()
    {
        for (int i = 0; i < btnsControl.Length; i++)
        {
            btnsControl[i].SetActive(true);
        }
        pausePanel.SetActive(false);
        storePanel.SetActive(false);
        gameManagers.isPaused = false;
        statsPanel.SetActive(false);
        UnPauseInGame();
    }

    public void OnMenu()
    {
        //Save.instance.SetSave();
        Save.instance.SaveJson();
        SceneManager.LoadScene(0);
    }
    
    public void OnInventoryEnter()
    {
        if(!InventoryPanel.activeSelf)
        {
            for (int i = 0; i < btnsControl.Length; i++)
            {
                btnsControl[i].SetActive(false);
            }
            btnsControl[5].SetActive(true);
            PauseInGame();                          //Pause in game
            InventoryPanel.SetActive(true);
        }
        else if(InventoryPanel.activeSelf)
        {
            for (int i = 0; i < btnsControl.Length; i++)
            {
                btnsControl[i].SetActive(true);
            }
            InventoryPanel.SetActive(false);
            infoInventoryPanel.SetActive(false);
            UnPauseInGame();
        }
    }


    public void OnRetry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnStoreBtnEnter()
    {
        for (int i = 0; i < btnsControl.Length; i++)
        {
            btnsControl[i].SetActive(false);
        }
        storePanel.SetActive(true);
        PauseInGame();
        lobbyPanel.SetActive(false);
    }

    public void OnLobby()
    {
        if (!lobbyPanel.activeSelf)
        {
            lobbyPanel.SetActive(true);
            PauseInGame();
            BtnsControll(false);
        }
        else if(lobbyPanel.activeSelf)
        {
            lobbyPanel.SetActive(false);
            UnPauseInGame();
            BtnsControll(true);
        }
    }

    public void OnStats()
    {
        BtnsControll(false);
        lobbyPanel.SetActive(false);
        statsPanel.SetActive(true);
    }

    public void OnNextLevel()
    {
        Save.instance.SaveJson();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }


    void BtnsControll(bool _bool)
    {
        for (int i = 0; i < btnsControl.Length; i++)
        {
            btnsControl[i].SetActive(_bool);
        }
    }

    void PauseInGame()
    {
        gameManagers.isPaused = true;
        Time.timeScale = 0;
    }
    void UnPauseInGame()
    {
        gameManagers.isPaused = false;
        Time.timeScale = 1;
    }

    void Update()
    {
        if(gameManagers._diePanel)
        {
            for (int i = 0; i < btnsControl.Length; i++)
            {
                btnsControl[i].SetActive(false);
            }
            gameManagers.isPaused = true;
            Time.timeScale = 0;
        }
    }

}
