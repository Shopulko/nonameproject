﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventorySlot : MonoBehaviour {

    public Image icon;

    Item item;

    public Button removeButton;
    
    private void Start()
    {
        //inventoryPanelInfo.SetActive(false);
        
    }


    public void AddItem(Item newItem)
    {
        item = newItem;
        icon.enabled = true;
        icon.sprite = item.icon;
        removeButton.interactable = true;
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    public void OnRemoveBtn()
    {
        GameManagers.instance.OnDrop(item);
        //Inventory.instance.RemoveItem(item);
    }

    
    public void SelectItem()
    {
        if(item != null)
        {
            item.Select();
        }
        if(item == null)
        {
            GameManagers.instance.DisableInventoryInfoPanel();
        }

    }
    
    public void UseItem()
    {
        if(item != null)
        {
            item.Use();
        }
    }


   


}
