﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour {

    public Item item;

    public void PickUp()
    {
        bool wasPickUped = Inventory.instance.AddItem(item);
        if(wasPickUped)
        {
            Destroy(this.gameObject);
        }
        Debug.Log(item.name + " was pick uped");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            PickUp();
        }
    }

}
