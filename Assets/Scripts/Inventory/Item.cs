﻿using UnityEngine;


[CreateAssetMenu(fileName ="New Item", menuName = "Item")]
public class Item : ScriptableObject {

    new public string name = "New Item";
    public Sprite icon = null;
    public int damage = 0;
    public float health = 0f;
    public float speed = 0f;
    public int price = 0;

    
    public virtual void Select()
    {
        GameManagers.instance.SelectedItem(this);
    }
    
    public virtual void Use()
    {
        PlayerManager.instance.UpdateStats(health, damage);
        GameManagers.instance.AddSpeed(speed);
        Inventory.instance.RemoveItem(this);
    }

    public virtual void RemoveItem()
    {
        Inventory.instance.RemoveItem(this);
    }


}
