﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class InventoryUI : MonoBehaviour {

    Inventory inventory;
    public Transform itemParent;

    InventorySlot[] slots;

    private void Awake()
    {
        inventory = Inventory.instance;
        inventory.onItemChangedCallBack += UpdateUI;
        slots = itemParent.GetComponentsInChildren<InventorySlot>();
    }

    void Start () {
        //inventory = Inventory.instance;
        //inventory.onItemChangedCallBack += UpdateUI;


        //slots = itemParent.GetComponentsInChildren<InventorySlot>();
	}
	
	
	void Update () {
		
	}


    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if(i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }



}
