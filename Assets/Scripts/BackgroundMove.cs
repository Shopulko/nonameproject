﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour {

    Vector3 velocity = Vector3.zero;
    public GameObject bg;
    public float moveSpeed;
    public Transform player;
	void Start () {
        
	}
	
	
	void FixedUpdate () {
        //moveSpeed = GameManagers.instance._moveSpeed;
        
        Vector3 posBg = new Vector3(bg.transform.position.x, bg.transform.position.y, bg.transform.position.z);
        //Debug.Log("moveSpeed = " + moveSpeed);
        if(moveSpeed > 0)
        {
            posBg.x = player.transform.position.x - 1f;
        }
        if(moveSpeed < 0)
        {
            posBg.x = player.transform.position.x + 1f;
        }

        bg.transform.position = Vector3.SmoothDamp(bg.transform.position, posBg, ref velocity, 2f);


	}
}
