﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyScript : MonoBehaviour {


    private int moneyAnount = 5;

	void Start () {
		
	}
	
	
	void Update () {
		
	}


    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.tag == "Player")
        {
            GameManagers.instance.AddMoney(moneyAnount);
            Destroy(this.gameObject);
            return;
        }
        if(other.collider.tag == "Money")
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }

}
