﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class Save : MonoBehaviour {

    #region Singleton
    public static Save instance;
    private void Awake()
    {
        if(instance != null)
        {
            Debug.Log("Err");
        }
        instance = this;
    }
    #endregion
    /*
        public void SetSave()
        {
            PlayerPrefs.SetInt("Money", GameManagers.instance.money);
        }

        public int GetSave()
        {
            return PlayerPrefs.GetInt("Money");
        }
    */


    public SaveClass saveClass = new SaveClass();

    private string path;

    [Header("Items for load")]
    public Item[] items;

    string[] itemsS;

    private void Start()
    {
        

//#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "Save.json");  
//#else
        //path = Path.Combine(Application.dataPath, "Save.json");
//#endif

        /*
        if (File.Exists(path))
        {
            saveClass = JsonUtility.FromJson<SaveClass>(File.ReadAllText(path));
            GameManagers.instance.money = saveClass.money;
            Inventory.instance.items = saveClass.items;
            if (Inventory.instance.onItemChangedCallBack != null)
            {
                Inventory.instance.onItemChangedCallBack.Invoke();
                Debug.Log("Invoke");
            }

        }
        else
        {
            GameManagers.instance.money = 0;
        }
        */

        LoadJson();
    }

    public void LoadJson()
    {
        if (File.Exists(path))
        {
            saveClass = JsonUtility.FromJson<SaveClass>(File.ReadAllText(path));
            GameManagers.instance.money = saveClass.money;
            GameManagers.instance.SetSpeed(saveClass.speed);
            GameManagers.instance.SetTimeBullet(saveClass.timeBullet);
            PlayerManager.instance.SetDamage(saveClass.damage);
            PlayerManager.instance.SetMaxHealth(saveClass.maxHealth);

            if (saveClass.items != null)
            {
                for (int i = 0; i < saveClass.items.Count; i++)
                {
                    for (int  j = 0;  j < items.Length;  j++)
                    {
                        if(saveClass.items[i] == items[j].name)
                        {
                            Inventory.instance.AddItem(items[j]);
                            break;
                        }
                    }
                    
                    Debug.Log(saveClass.items[i]);
                }
            }
        }
    }


    public void SaveJson()
    {
        saveClass.money = GameManagers.instance.money;
        saveClass.speed = GameManagers.instance.GetSpeed();
        saveClass.timeBullet = GameManagers.instance.GetTimeBullet();
        saveClass.damage = PlayerManager.instance.GetDamage();
        saveClass.maxHealth = PlayerManager.instance.GetMaxHealth();
        saveClass.items.Clear();
        for (int i = 0; i < Inventory.instance.items.Count; i++)
        {
            saveClass.items.Add(Inventory.instance.items[i].name);
            Debug.Log(Inventory.instance.items[i].name);
        }
        File.WriteAllText(path, JsonUtility.ToJson(saveClass));

    }

    public void DeleteJson()
    {
        File.Delete(path);
    }
}


[Serializable]
public class SaveClass
{
    public int money;
    public List<string> items = new List<string>();
    public float speed;
    public float timeBullet;
    public int damage;
    public float maxHealth;

}
