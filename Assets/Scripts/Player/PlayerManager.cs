﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {


    public float maxHealth = 100f;
    public float currentHealth;
    public int damage = 1;

    public GameObject heathPanel;
    Vector3 scale;

    public Text healthText;

    #region Singleton
    public static PlayerManager instance;

    private void Awake()
    {
        if(instance != null)
        {
            Debug.Log("More plScript");
        }
        instance = this;
    }
    #endregion

    Animator anim;

    public GameObject[] ignoreObject;


    void Start () {
        currentHealth = maxHealth;
        scale = heathPanel.transform.localScale;
        Time.timeScale = 1;
        UpdateHealthBar();
        anim = GetComponent<Animator>();
        
    }
	
	
	void Update () {
		if(currentHealth <= 0)
        {
            currentHealth = 0;
            GameManagers.instance.PlayerDie();
        }

        if(GameManagers.instance.btnLeftMove.isClicked || GameManagers.instance.btnRightMove.isclicked)
        {
            anim.SetBool("IsWalked", true);
        }
        else
        {
            anim.SetBool("IsWalked", false);
        }

	}


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Item")
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
        }
        if (collision.gameObject.tag == "Money")
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
        }
    }


    public void GetDamage(float _damage)
    {
        currentHealth -= _damage;
        Debug.Log("playerHealth = " + currentHealth);
        anim.SetBool("IsDamaged", true);
        UpdateHealthBar();
    }

    public void UpdateStats(float _health, int _damage)
    {
        currentHealth += _health;
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        damage += _damage;
        UpdateHealthBar();
    }

    public void UpdateStats(float _health)
    {
        currentHealth += _health;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        UpdateHealthBar();
    }

    public void UpdateStats(int _damage)
    {
        damage += _damage;
    }

    void UpdateHealthBar()
    {
        scale.x = currentHealth / maxHealth;
        heathPanel.transform.localScale = scale;
        if(currentHealth >= 75)
        {
            heathPanel.GetComponent<Image>().color = Color.green;
        }
        else if(currentHealth < 75 && currentHealth >= 50)
        {
            heathPanel.GetComponent<Image>().color = Color.yellow;
        }
        else if (currentHealth < 50 && currentHealth > 25)
        {
            heathPanel.GetComponent<Image>().color = Color.white;
        }
        else
        {
            heathPanel.GetComponent<Image>().color = Color.red;
        }
        healthText.text = currentHealth.ToString();
    }


    public float GetHealth()
    {
        return currentHealth;
    }

    public void FalseBool()
    {
        anim.SetBool("IsDamaged", false);
    }


    public int GetDamage()
    {
        return damage;
    }

    public void SetDamage(int _damage)
    {
        damage = _damage;
    }

    public void AddMaxHealth(float _add)
    {
        maxHealth += _add;
    }
    public void SetMaxHealth(float _maxH)
    {
        maxHealth = _maxH;
    }

    public float GetMaxHealth()
    {
        return maxHealth;
    }

}
