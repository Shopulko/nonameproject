﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour {

    GameManagers gameManagers;

    public bool isLoookRight = true;
    Rigidbody2D rb;

    public float speed;
    public float jumpForce = 10f;
    

    public bool isGroundet;
    public LayerMask layerMask;
    public Transform groundPoint;

    Animator anim;

    public float moveSpeed;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        gameManagers = GameManagers.instance;
        anim = GetComponent<Animator>();
	}
	
	
	void Update () {

        isGroundet = Physics2D.OverlapCircle(groundPoint.position, 0.15f, layerMask);
        speed = gameManagers.GetSpeed();
		if(gameManagers.btnRightMove.isclicked)
        {
            if (!isLoookRight)
            {
                transform.Rotate(new Vector3(0, -180, 0));
                isLoookRight = true;
                gameManagers.isPlayerLoockRight = true;
            }
            moveSpeed = speed * Time.deltaTime * 20f;
            gameManagers._moveSpeed = moveSpeed;
            rb.velocity = new Vector2(moveSpeed, rb.velocity.y);
        }

        if(gameManagers.btnLeftMove.isClicked)
        {
            if (isLoookRight)
            {
                transform.Rotate(new Vector3(0, 180, 0));
                isLoookRight = false;
                gameManagers.isPlayerLoockRight = false;
            }
            moveSpeed = -speed * Time.deltaTime * 20f;
            //Debug.Log(moveSpeed);
            gameManagers._moveSpeed = moveSpeed;
            rb.velocity = new Vector2(moveSpeed, rb.velocity.y); 
        }
        moveSpeed = 0f;
        //gameManagers._moveSpeed = moveSpeed;
    }

    public void OnJump()
    {
        if(isGroundet)
            rb.velocity = new Vector2(0, jumpForce);
    }

    public void OnRightMove()
    {
        if (!isLoookRight)
        {
            transform.Rotate(new Vector3(0, -180, 0));
            isLoookRight = true;
            gameManagers.isPlayerLoockRight = true;
        }
        rb.velocity = new Vector2(speed * Time.deltaTime * 20f, rb.velocity.y);
    }

   
}
