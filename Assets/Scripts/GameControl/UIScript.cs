﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScript : MonoBehaviour {

    public Text scoreText;
    public GameObject inventory;
	public TextMeshProUGUI moneyText;
    //public TextMeshProUGUI wavesCounter;
	
	void Update () {
        scoreText.text = "Your score is " + GameManagers.instance.score.ToString();
        moneyText.text = GameManagers.instance.money.ToString();
        //wavesCounter.text = "Waves " + GameManagers.instance._waves.ToString();
	}

}
