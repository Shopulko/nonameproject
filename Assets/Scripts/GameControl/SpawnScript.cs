﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {


    public GameObject[] enemyPrefab;
    public Transform[] spawnPoints;

    public float timeBetweenSpawn = 5f;
    public float waveTimeSpawn;
    

    private enum SpawnState { SPAWNING, WAITING, COUTING};
    private SpawnState state = SpawnState.COUTING;

    int waves = 1;

    int enemyCount;

    int currEnemy = 0;

	void Start () {
        waveTimeSpawn = timeBetweenSpawn;
        enemyCount = GameManagers.instance._enemyCount;
    }
	
	
	void Update () {
		
        if(waveTimeSpawn <= 0)
        {
            if(currEnemy < enemyCount)
            {
                if (state != SpawnState.SPAWNING)
                {
                    StartCoroutine(SpawnTimer());
                    waveTimeSpawn = timeBetweenSpawn * waves;
                    //++waves;
                    //GameManagers.instance._waves = waves;
                }
            }
           
        }
        else
        {
            waveTimeSpawn -= Time.deltaTime;
            
        }
        if(currEnemy > enemyCount)
        {
            state = SpawnState.COUTING;
        }
	}


    IEnumerator SpawnTimer()
    {
        state = SpawnState.SPAWNING;

        for (int i = 0; i < enemyCount; i++)
        {
            Spawn();
            yield return new WaitForSeconds(4f);
            
        }

        state = SpawnState.WAITING;
        ++waves;
        GameManagers.instance._waves = waves;
        yield break;

    }


    void Spawn()
    {
        int enemy = enemyPrefab.Length;
        int enemyRand = Random.Range(0, enemy);

        int pointRand = Random.Range(0, 2);

        Instantiate(enemyPrefab[enemyRand], spawnPoints[pointRand].position, spawnPoints[pointRand].rotation);
        currEnemy++;
            
    }

}
