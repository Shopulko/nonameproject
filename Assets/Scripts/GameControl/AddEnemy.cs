﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddEnemy : MonoBehaviour {

    public GameObject enemyPrefab;
    public Transform[] spawnPoints;

    int count = 0;

    public void OnSpawnBtnEnter()
    {
        Spawn();
    }


    void Spawn()
    {
        if(count == 0)
        {
            Instantiate(enemyPrefab, spawnPoints[0].position, spawnPoints[0].rotation);
            count = 1;
        }
        else if (count == 1)
        {
            Instantiate(enemyPrefab, spawnPoints[1].position, spawnPoints[1].rotation);
            count = 0;
        }
        else
        {
            Instantiate(enemyPrefab, spawnPoints[0], true);
        }
    }


}
