﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class GameManagers : MonoBehaviour {

    #region Singleton
    public static GameManagers instance;

    private void Awake()
    {
        if(instance != null)
        {
            Debug.Log("more that one instance on scene");
            return;
        }
        instance = this;

        _enemyCount = lvl.enemyCount; // enemy Counter on level

    }
    #endregion

    public BtnRightMove btnRightMove;
    public BtnLeftMove btnLeftMove;
    public bool isPlayerLoockRight = true;


    public int score = 0;
    public bool enemyAttack = false;

    public bool isPaused = false;

    public GameObject diePanel;
    public bool _diePanel = false;

    
    public GameObject inventoryPanelInfo;
    public Image iconInfo;
    public TextMeshProUGUI textInfo;

    public GameObject DropPanel;
    public TextMeshProUGUI textDrop;

    public GameObject winPanel;

    public int money = 0;

    public int _waves = 0;

    Item selectedItem;

    public float _speed = 10f;
    public float _timeBullet = 0.5f;

    public float _moveSpeed;

    public LevelScript lvl;
    public int _enemyCount;
    public int enemyDienumber = 0;


    

    void Start () {
        Time.timeScale = 1;
        diePanel.SetActive(false);
        inventoryPanelInfo.SetActive(false);
        
	}
	
	
	void Update () {
		if(enemyDienumber == _enemyCount)
        {
            StartCoroutine(OnWinLvl());
            isPaused = true;
        }
	}

    public void PlayerDie()
    {
        diePanel.SetActive(true);
        _diePanel = true; 
    }

    
    public void OnSelectedItem(Item item)
    {
        iconInfo.sprite = null;
        textInfo.text = null;
        inventoryPanelInfo.SetActive(true);
        iconInfo.sprite = item.icon;
        if(item.damage != 0)
        {
            textInfo.text += "Damage +" + item.damage;
        }
        if(item.health != 0)
        {
            textInfo.text += "\nHeath +" + item.health;
        }
        if(item.speed !=0)
        {
            textInfo.text += "\nSpeed +" + item.speed;
        }
    }
    
    public void SelectedItem(Item item)
    {
        selectedItem = item;
        if(selectedItem != null)
            OnSelectedItem(selectedItem);
        if (selectedItem == null)
        {
            inventoryPanelInfo.SetActive(false);
        }
            
    }

    public void DisableInventoryInfoPanel()
    {
        inventoryPanelInfo.SetActive(false);
    }

    public void UseItem()
    {
        selectedItem.Use();
        inventoryPanelInfo.SetActive(false);
        iconInfo.sprite = null;
        textInfo.text = null;
        inventoryPanelInfo.SetActive(false);
    }

    public void OnDrop(Item _item)
    {
        selectedItem = _item;
        DropPanel.SetActive(true);
        textDrop.text = "Do you want drop " + selectedItem.name + "?";
    }

    public void OnDropBtn()
    {
        Inventory.instance.RemoveItem(selectedItem);
        DropPanel.SetActive(false);
    }

    public void OnNoDrop()
    {
        DropPanel.SetActive(false);
    }

    public void AddMoney(int _money)
    {
        money += _money;
    }



    #region Set/Get
    public void AddSpeed(float _speed_)
    {
        _speed += _speed_;
    }

    public void SetSpeed(float _speed_)
    {
        _speed = _speed_;
    }

    public float GetSpeed()
    {
        return _speed;
    }

    public void AddTimeBullet(float _timeBull_)
    {
        _timeBullet += _timeBull_;
    }

    public void SetTimeBullet(float _time)
    {
        _timeBullet = _time;
    }

    public float GetTimeBullet()
    {
        return _timeBullet;
    }
    #endregion 



    /*
    void OnWinLvl()
    {
        winPanel.SetActive(true);
        //Save.instance.SetSave();
    }
    */

    IEnumerator OnWinLvl()
    {
        yield return new WaitForSeconds(5f);
        winPanel.SetActive(true);
        Time.timeScale = 0;
    }

    /*
    void GetSave()
    {
        if(Save.instance.GetSave() == null)
        {
            money = 0;
        }
        else
        {
            money = Save.instance.GetSave();
        }
    }
    */

    public void OnDeleteKey()
    {
        Save.instance.DeleteJson();
    }

}
