﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    public GameObject lvlPanel;
    public Scrollbar loadingBar;

    private void Start()
    {
        lvlPanel.SetActive(false);
        loadingBar.gameObject.SetActive(false);
    }

    public void OnPlayEnter()
    {
        lvlPanel.SetActive(true);
    }

    public void OnExitEnter()
    {
        Application.Quit();
    }

    public void OnBtnLvl(int _scene)
    {
        loadingBar.gameObject.SetActive(true);
        StartCoroutine(AsyncLoad(_scene));
    }

    IEnumerator AsyncLoad(int scene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);
        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            loadingBar.size = progress;
            yield return null;
        }
    }

}
