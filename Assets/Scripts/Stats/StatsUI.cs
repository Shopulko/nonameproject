﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class StatsUI : MonoBehaviour {


    public TextMeshProUGUI speedText;
    public TextMeshProUGUI bulletText;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI healthText;

    public Button speedBtn;
    public Button bulletsUpdateBtn;
    public Button damageBtn;
    public Button healthBtn;

    private int _speed = 0;
    private int _bullet = 0;
    private int _damage = 0;
    private int _health = 0;

    void Start () {
		
	}
	
	
	void Update () {
        speedText.text = "Speed : " + GameManagers.instance.GetSpeed().ToString("0.0");
        bulletText.text = "Time between bullets : " + GameManagers.instance.GetTimeBullet().ToString("0.00");
        damageText.text = "Damage : " + PlayerManager.instance.GetDamage().ToString();
        healthText.text = "MaxHealth : " + PlayerManager.instance.GetMaxHealth().ToString("0");


        if(_speed >= 5)
        {
            speedBtn.interactable = false;
        }
        if (_damage >= 5)
        {
            damageBtn.interactable = false;
        }
        if (_bullet >= 5)
        {
            bulletsUpdateBtn.interactable = false;
        }
        if (_health >= 5)
        {
            healthBtn.interactable = false;
        }
    }


    public void OnSpeedUpgrade()
    {
        if(GameManagers.instance.money >= 40)
        {
            GameManagers.instance.AddSpeed(0.2f);
            _speed++;
        }
        else
        {
            Debug.Log("Nit enough money");
        }
    }

    public void OnTimeBullets()
    {
        if (GameManagers.instance.money >= 40)
        {
            GameManagers.instance.AddTimeBullet(-0.02f);
            _bullet++;
        }
        else
        {
            Debug.Log("Nit enough money");
        }
       
    }

    public void OnDamageUpdate()
    {
        if (GameManagers.instance.money >= 40)
        {
            PlayerManager.instance.UpdateStats(1);
            _damage++;
        }
        else
        {
            Debug.Log("Nit enough money");
        }
        
    }

    public void OnHealthUpdate()
    {
        if (GameManagers.instance.money >= 40)
        {
            PlayerManager.instance.AddMaxHealth(5f);
            _health++;
        }
        else
        {
            Debug.Log("Nit enough money");
        }
       
    }


}
