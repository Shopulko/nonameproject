﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnim : MonoBehaviour
{

    Transform playerPosition;

    public bool isLookingRight = true;
    public float speed = 5f;

    float maxHealthEnemy = 100f;
    public float heathEnemy = 100f;
    public float damageEnemy = 5f;
    public float getdamage = 15f;

    Vector3 stayVector;

    GameManagers gameManagers;
    PlayerManager playerManager;

    //public GameObject EnemyHealthUI;
    Vector3 scale;


    public GameObject moneyPref;

    float timeBetweenAttackfull = 2f;
    public float timeBetweenAttack = 2f;
    bool canAttack = true;

    public GameObject[] items;

    Animator anim;

    void Start()
    {
        //scale = EnemyHealthUI.transform.localScale;
        gameManagers = GameManagers.instance;
        playerManager = PlayerManager.instance;

        anim = GetComponent<Animator>();

    }


    void Update()
    {

        scale.x = heathEnemy / maxHealthEnemy;
        //EnemyHealthUI.transform.localScale = scale;



        playerPosition = playerManager.gameObject.transform;
        if (!gameManagers.isPaused)
        {
            anim.SetBool("Walk", true);
            Flip();
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(playerPosition.position.x, transform.position.y, transform.position.z), speed * Time.deltaTime);
        }

        if (heathEnemy <= 0)
        {
            DieEnemy(true);
        }

        if (gameManagers.isPaused)
        {
            StayOnPlace();
        }

        if (!canAttack)
        {
            timeBetweenAttack -= Time.deltaTime;
            if (timeBetweenAttack <= 0)
            {
                canAttack = true;
                timeBetweenAttack = timeBetweenAttackfull;
            }
        }

        /*
                if(timeBetweenAttack <= 0)
                {
                    canAttack = true;
                    timeBetweenAttack = timeBetweenAttackfull;
                }
        */


    }


    void Flip()
    {
        if (playerPosition.position.x < transform.position.x && isLookingRight)
        {
            transform.Rotate(0, 180, 0);
            isLookingRight = false;
        }
        if (playerPosition.position.x > transform.position.x && !isLookingRight)
        {
            transform.Rotate(0, -180, 0);
            isLookingRight = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Bullet")
        {
            heathEnemy -= getdamage + playerManager.damage;
            gameManagers.score += 5;
            //gameManagers.enemyAttack = true;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {

            Attack();
            //playerManager.GetDamage(25f);
            //DieEnemy(false);
        }
        if (collision.collider.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            transform.position = new Vector2(transform.position.x + 0.5f, transform.position.y);
        }
    }

    public void AttackFalse()
    {
        anim.SetBool("Attack", false);
        playerManager.GetDamage(damageEnemy);
        canAttack = false;
        
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            Attack();
        }
    }


    void DieEnemy(bool _dieOutBullets)
    {
        Debug.Log("Enemy died");
        anim.SetBool("Die", true);


        /*
        Drop();

        if (_dieOutBullets)             // If die out Bullet add money on the scene
        {
            MoneyPush();
        }
        gameManagers.enemyDienumber++;
        Destroy(this.gameObject);
        */
    }


    public void DieFalse()
    {
        Drop();
        MoneyPush();
        gameManagers.enemyDienumber++;
        Destroy(this.gameObject);
    }

    void StayOnPlace()
    {
        stayVector = transform.position;
        transform.position = stayVector;
    }

    void MoneyPush()                    // Add money on the scene out Enemy
    {
        Vector3 vec = transform.position;
        int number = Random.Range(1, 5);
        for (int i = 0; i < number; i++)
        {
            vec.x += 0.1f;
            Instantiate(moneyPref, vec, transform.rotation);
        }
    }

    void Attack()
    {
        if (canAttack)
        {
            StayOnPlace();
            anim.SetBool("Attack", true);
        }
    }

    void Drop()
    {
        float rand = Random.Range(0, 50);
        if (rand <= 30)
        {
            int _randItem = Random.Range(0, items.Length);
            Instantiate(items[_randItem], transform.position, Quaternion.identity);
        }
    }

}
