﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 10f;
    bool right;
	void Start () {
        StartCoroutine(BulletDie());
        right = GameManagers.instance.isPlayerLoockRight;
	}
	
	
	void Update () {
        if(right)
        {
            transform.Translate(transform.right * speed * Time.deltaTime);
        }
        if(!right)
        {
            transform.Translate(-transform.right * speed * Time.deltaTime);
        }
	}



    IEnumerator BulletDie()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
            
        if (other.tag == "Cordone")
        {
            Destroy(this.gameObject);
        }
    }


}
