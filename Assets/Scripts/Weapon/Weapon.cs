﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    Transform spawnPoint;
    public GameObject bulletPrefab;
    public float timeBetweenFire;

    private float timeFire = 0.5f;

    bool isEnterened = false;

    bool isFirst = true;

    private void Start()
    {
        timeBetweenFire = timeFire;
        spawnPoint = GetComponent<Transform>();
    }

    private void Update()
    {
        timeFire = GameManagers.instance.GetTimeBullet();
        if(isEnterened)
        {
            if(isFirst)
            {
                OnFireBtn();
                isFirst = false;
            }
            timeBetweenFire -= Time.deltaTime;
            if (timeBetweenFire <= 0)
            {
                OnFireBtn();
                timeBetweenFire = timeFire;
            }
        }
    }

    public void OnFireBtn()
    {
        Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation);
    }

    public void OnFireEnter()
    {
        isEnterened = true;
    }

    public void OnFireUp()
    {
        isEnterened = false;
        isFirst = true;
        timeBetweenFire = timeFire;
    }

}
